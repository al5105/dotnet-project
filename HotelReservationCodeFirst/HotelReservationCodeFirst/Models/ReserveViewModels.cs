﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;

using System.Web.Mvc; //for SelectListItem

namespace HotelReservationCodeFirst.Models
{
    //view models for Reserve view 
    public class ReserveViewModel
    {
        //[Required]
        //public string roomType { get; set; }

        //public IEnumerable<SelectListItem> rooms { get; set; }

        //available room numbers of each type of the room
        public int kingLeft { get; set; }
        public int queenLeft { get; set; }
        public int doubleLeft { get; set; }
        public int suiteLeft { get; set; }


        [Required]
        public DateTime checkInDate { get; set; }

        [Required]
        public DateTime checkOutDate { get; set; }

        [Display(Name = "King")]
        public string King;

        [Display(Name = "Queen")]
        public string Queen;

        [Display(Name = "Double")]
        public string Double;

        [Display(Name = "Suite")]
        public string Suite;

        [Required]
        public string roomType { get; set; }

        [Required]
        [Display(Name = "First name of guest 1:")]
        public string Guest1FirstName { set; get; }

        [Required]
        [Display(Name = "Last name of guest 1:")]
        public string Guest1LastName { set; get; }

        [Display(Name = "First name of guest 2:")]
        public string Guest2FirstName { set; get; }

        [Display(Name = "Last name of guest 2:")]
        public string Guest2LastName { set; get; }

        [Display(Name = "First name of guest 3:")]
        public string Guest3FirstName { set; get; }

        [Display(Name = "Last name of guest 3:")]
        public string Guest3LastName { set; get; }

        [Display(Name = "First name of guest 4:")]
        public string Guest4FirstName { set; get; }

        [Display(Name = "Last name of guest 4:")]
        public string Guest4LastName { set; get; }

    }

    //view model for confirming reservation view
    public class ConfirmReservationViewModel
    {
        [Required]
        [Display(Name = "First name")]
        public string firstName { set; get; }

        [Required]
        [Display(Name = "Last name")]
        public string lastName { set; get; }

        [Required]
        [Display(Name = "Address")]
        public string address { set; get; }

        [Required]
        [Display(Name = "City")]
        public string city { set; get; }

        [Required]
        [Display(Name = "State")]
        public string state { set; get; }

        [Required]
        [Display(Name = "Zip code")]
        public string zipCode { set; get; }

        [Required]
        [Display(Name = "Email address")]
        public string email { set; get; }

        [Required]
        [Display(Name = "Telephone number")]
        public string phone { set; get; }

    }

    public class ReservationInfoViewModel
    {
        //the total prize of the resrevation
        public double prize { get; set; }

    }
}