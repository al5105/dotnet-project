// <auto-generated />
namespace HotelReservationCodeFirst.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class updateroom : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(updateroom));
        
        string IMigrationMetadata.Id
        {
            get { return "201705090112199_update room"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
