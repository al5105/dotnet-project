﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HotelReservationCodeFirst.Models;

using DataModel;

using BizLogic;
using Microsoft.AspNet.Identity;

namespace HotelReservationCodeFirst.Controllers
{
    public class ReserveController : Controller
    {

        //Get
        public ActionResult Index()
        {
            string cIn = "05/10/2017";
            string cOut = "05/10/2017";

            //get the check in/out date
            DateTime checkIn = Convert.ToDateTime(cIn);
            DateTime checkOut = Convert.ToDateTime(cOut);

            //call bizLogic to get the number of available rooms
            ReserveViewModel model = new ReserveViewModel();
            HotelReservationExtensions logic = new HotelReservationExtensions();

            //add room series from checkn in date to check out date to the database
            logic.addRoomSeries(0, checkIn, checkOut);
            logic.addRoomSeries(1, checkIn, checkOut);
            logic.addRoomSeries(2, checkIn, checkOut);
            logic.addRoomSeries(3, checkIn, checkOut);

            //check the number of each type of rooms available
            model.kingLeft = logic.roomAvailable(0, checkIn, checkOut);
            model.queenLeft = logic.roomAvailable(1, checkIn, checkOut);
            model.doubleLeft = logic.roomAvailable(2, checkIn, checkOut);
            model.suiteLeft = logic.roomAvailable(3, checkIn, checkOut);


            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ReserveViewModel model)
        {
            string cIn = Request.Cookies["CheckInDate"].Value;
            string cOut = Request.Cookies["CheckOutDate"].Value;

            //hard code the check in/out date
            Session["CheckInDate"] = cIn;
            Session["CheckOutDate"] = cOut;


            //Response.Cookies["Reservation"]["CheckInDate"] = "05/09/2017";
            //Response.Cookies["Reservation"]["CheckOutDate"] = "05/12/2017";
            HotelReservationExtensions logic = new HotelReservationExtensions();
            DateTime checkIn = Convert.ToDateTime(cIn);
            DateTime checkOut = Convert.ToDateTime(cOut);

            logic.addRoomSeries(0, checkIn, checkOut);
            logic.addRoomSeries(1, checkIn, checkOut);
            logic.addRoomSeries(2, checkIn, checkOut);
            logic.addRoomSeries(3, checkIn, checkOut);


            //add room type to cookies
            Response.Cookies["Reservation"]["RoomType"] = model.roomType;
            Session["RoomType"] = model.roomType;

            Response.Cookies["Reservation"]["Guest1FirstName"] = model.Guest1FirstName;
            Response.Cookies["Reservation"]["Guest1LastName"] = model.Guest1LastName;

            Response.Cookies["Reservation"]["Guest2FirstName"] = model.Guest2FirstName;
            Response.Cookies["Reservation"]["Guest2LastName"] = model.Guest2LastName;

            Response.Cookies["Reservation"]["Guest3FirstName"] = model.Guest3FirstName;
            Response.Cookies["Reservation"]["Guest3LastName"] = model.Guest3LastName;

            Response.Cookies["Reservation"]["Guest4FirstName"] = model.Guest3FirstName;
            Response.Cookies["Reservation"]["Guest4LastName"] = model.Guest3LastName;

            return RedirectToAction("ConfirmReservation");

        }

        public ActionResult ConfirmReservation()
        {
            //for the grid view
            var northwind = new HotelDataModel();
            //Get the Products entities and add them to the ViewBag.
            ViewBag.Rooms = northwind.RoomSet;


            return View();
        }

        [HttpPost]
        //page for confirming reservation and setting billing information
        public ActionResult ConfirmReservation(ConfirmReservationViewModel model)
        {

            Random random = new Random();
            int confirmationNum = random.Next(1, 1000000);

            //hard code the check in/out date
            //DateTime checkInDate = Convert.ToDateTime(Request.Cookies["Reservation"]["CheckInDate"]);
            //DateTime checkOutDate = Convert.ToDateTime(Request.Cookies["Reservation"]["CheckOutDate"]);

            DateTime checkInDate = Convert.ToDateTime((string)Session["CheckInDate"]);
            DateTime checkOutDate = Convert.ToDateTime((string)Session["CheckOutDate"]);


            bool checkedIn = false;
            bool checkedOut = false;
            string address = model.address;
            string city = model.city;
            string state = model.state;
            string zip = model.zipCode;
            string email = model.email;
            string phone = model.phone;

            string billingFirstName = model.firstName;
            string billingLastName = model.lastName;

            //hard code the rest names
            string firstName2 = "firstName2";
            string firstName3 = "firstName3";
            string firstName4 = "firstName4";
            string lastName2 = "lastName2";
            string lastName3 = "lastName3";
            string lastName4 = "lastName4";
            

            //hard code the room type 
            string room = Request.Cookies["Reservation"]["RoomType"];
            int Room_type = 0;
            if(room == "king")
            {
                Room_type = 0;
            }
            else if(room == "queen")
            {
                Room_type = 1;
            }
            else if(room == "double")
            {
                Room_type = 2;
            }
            else
            {
                Room_type = 3;
            }

            //save billing information to Cookies
            //dangerous!
            Response.Cookies["Reservation"]["FirstName"] = billingFirstName;
            Response.Cookies["Reservation"]["LastName"] = billingLastName;

            Response.Cookies["Reservation"]["ConfirmationNumber"] = ""+ confirmationNum;
            Response.Cookies["Reservation"]["BillingFirstName"] = model.firstName;
            Response.Cookies["Reservation"]["BillingLastName"] = model.lastName;
            Response.Cookies["Reservation"]["Phone"] = model.phone;
            Response.Cookies["Reservation"]["Email"] = model.email;


            //add the reservation to database using business logic
            HotelReservationExtensions biz = new HotelReservationExtensions();
            var userId = User.Identity.GetUserId();
            Reservation reservation = biz.customerReserve(confirmationNum, checkInDate, checkOutDate, checkedIn, checkedOut,
                address, city, state, zip, email, phone, billingFirstName, billingLastName, firstName2,
                lastName2, firstName3, lastName3, firstName4, lastName4, Room_type, userId);

            //change the occupancy of the room
            biz.changeOccupancy(Room_type, checkInDate, checkOutDate);

            //if user is logged in, add this reservation to the user
            bool loggedIn = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            if (loggedIn)
            {
                string id = User.Identity.GetUserId();
                CustomerController cc = new CustomerController();
                cc.addReservation(id, reservation);
            }

            return RedirectToAction("ReservationInfo");

        }

        //Get
        public ActionResult ReservationInfo(ReservationInfoViewModel model)
        {
            DateTime checkIn = Convert.ToDateTime((string)Session["CheckIndate"]);
            DateTime checkOut = Convert.ToDateTime((string)Session["CheckOutdate"]);

            double day = (checkOut.Date - checkIn.Date).TotalDays;

            //hard code the total prize of this reservation
            int room_type = 0;

            if ((string)Session["RoomType"] == "king")
            {
                room_type = 0;
            }
            else if ((string)Session["RoomType"] == "Queen")
            {
                room_type = 1;
            }
            else if ((string)Session["RoomType"] == "Double")
            {
                room_type = 2;
            }
            else
            {
                room_type = 3;
            }

            HotelReservationExtensions biz = new HotelReservationExtensions();
            model.prize = (int)biz.getTotalRateForDayRange(room_type, checkIn, checkOut);
            return View(model);
        }

    }
}