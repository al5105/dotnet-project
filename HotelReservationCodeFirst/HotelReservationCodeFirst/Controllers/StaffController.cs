﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BizLogic;
using HotelReservationCodeFirst.Models;
using HotelReservationCodeFirst.Controllers;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace HotelReservationSystem.Controllers
{
    [Authorize(Roles = "staff")]
    public class StaffController : Controller
    {
        // GET: Staff
        public ActionResult Index()
        {          
                return View();           
        }

        // TODO: [StaffAuthorize]
        public ActionResult StaffViewInventory()
        {
            HashSet<Room> roomSet_copy = new HashSet<Room>();

            using (var context = new HotelDataModel())
            {
                /*
                foreach (Room r1 in context.RoomSet)
                {
                    Room r2 = new Room();
                    r2.type = r1.type;
                    r2.basicPrice = r1.type;
                    r2.occupancy = r1.occupancy;
                    r2.maxGuest = r1.maxGuest;
                    r2.description = r1.description;
                    r2.amanities = r1.amanities;
                    r2.inventory = r1.inventory;

                    roomSet_copy.Add(r2);
                }
                */
                var room_query0 = from r in context.RoomSet
                                  where r.type == 0
                                  select r;
                var room0 = room_query0.FirstOrDefault();
                if (room0 != null)
                    roomSet_copy.Add(room0);

                var room_query1 = from r in context.RoomSet
                                  where r.type == 1
                                  select r;
                var room1 = room_query1.FirstOrDefault();
                if (room1 != null)
                    roomSet_copy.Add(room1);

                var room_query2 = from r in context.RoomSet
                                  where r.type == 2
                                  select r;
                var room2 = room_query2.FirstOrDefault();
                if (room2 != null)
                    roomSet_copy.Add(room2);

                var room_query3 = from r in context.RoomSet
                                  where r.type == 3
                                  select r;
                var room3 = room_query3.FirstOrDefault();
                if (room3 != null)
                    roomSet_copy.Add(room3);

                return View(roomSet_copy);
            }
        }

        // TODO: [StaffAuthorize]
        public ActionResult EditInventory(int id)
        {
            using (var context = new HotelDataModel())
            {
                var room_query = from r in context.RoomSet
                                 where r.type == id
                                 select r;
                var room = room_query.FirstOrDefault();
                return View(room);
            }
        }

        // TODO: [StaffAuthorize]
        [HttpPost]
        public ActionResult EditInventory(Room r)
        {
            // TODO
            using (var context = new HotelDataModel())
            {
                var room_query = from ro in context.RoomSet
                                 where ro.type == r.type
                                 select ro;
                //    var room = room_query.FirstOrDefault();
                //    room.inventory = r.inventory;
                foreach (Room room in room_query)
                {
                    room.inventory = r.inventory;
                }

                context.SaveChanges();
            }
            return RedirectToAction("StaffViewInventory");
        }

        // TODO: StaffAu
        public ActionResult CheckInHelper(Reservation res)
        {
            var num = res.confirmationNum;
            Session["confirmationNum"] = num;
            return RedirectToAction("CheckIn");
        }

        // TODO: [StaffAuthorize]
        public ActionResult CheckIn()
        {
            //  int confirmationNum = Convert.ToInt32(Request.Cookies["confirmationNum"].ToString());
            int confirmationNum = Convert.ToInt32(Session["confirmationNum"].ToString());
            using (var context = new HotelDataModel())
            {
                var checkInReservation_query = from r in context.ReservationSet
                                               where r.confirmationNum == confirmationNum
                                               select r;
                var checkInReservation = checkInReservation_query.FirstOrDefault();
                //   HashSet<Reservation> rs = new HashSet<Reservation>();
                //   rs.Add(checkInReservation);
                //   return View(rs);
                return View(checkInReservation);
            }
        }

        // TODO: [StaffAuthorize]
        [HttpPost]
        public ActionResult CheckIn(Reservation reservation)
        {
            int confirmationNum = Convert.ToInt32(Session["confirmationNum"].ToString());
            using (var context = new HotelDataModel())
            {
                var checkInReservation_query = from r in context.ReservationSet
                                               where r.confirmationNum == confirmationNum
                                               select r;
                var checkInReservation = checkInReservation_query.FirstOrDefault();
                checkInReservation.checkedIn = true;
                checkInReservation.checkInDate = DateTime.Now.Date;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        // TODO: StaffAu
        public ActionResult CheckOutHelper(Reservation res)
        {
            var num = res.confirmationNum;
            Session["confirmationNum"] = num;
            return RedirectToAction("CheckOut");
        }
        // TODO: [StaffAuthorize]
        public ActionResult CheckOut()
        {
            //  int confirmationNum = Convert.ToInt32(Request.Cookies["confirmationNum"].ToString());
            int confirmationNum = Convert.ToInt32(Session["confirmationNum"].ToString());
            using (var context = new HotelDataModel())
            {
                var checkOutReservation_query = from r in context.ReservationSet
                                                where r.confirmationNum == confirmationNum
                                                select r;
                var checkOutReservation = checkOutReservation_query.FirstOrDefault();
                // HashSet<Reservation> rs = new HashSet<Reservation>();
                // rs.Add(checkOutReservation);
                // return View(rs);
                return View(checkOutReservation);
            }
        }

        // TODO: [StaffAuthorize]
        [HttpPost]
        public ActionResult CheckOut(Reservation reservation)
        {
            int confirmationNum = Convert.ToInt32(Session["confirmationNum"].ToString());
            using (var context = new HotelDataModel())
            {
                var checkOutReservation_query = from r in context.ReservationSet
                                                where r.confirmationNum == confirmationNum
                                                select r;
                var checkOutReservation = checkOutReservation_query.FirstOrDefault();
                checkOutReservation.checkedOut = true;
                checkOutReservation.checkOutDate = DateTime.Now.Date;
                context.SaveChanges();

                /*5/8: When check out, decrease today's occupancy */
                var room_query = from ro in context.RoomSet
                                 where ro.type == checkOutReservation.Room_type.type &&
                                 ro.date.Year == DateTime.Now.Year &&
                                 ro.date.Month == DateTime.Now.Month &&
                                 ro.date.Day == DateTime.Now.Day
                                 select ro;
                var roomTd = room_query.FirstOrDefault();
                roomTd.occupancy--;
                /*5/8: */

                // put info into Session for billing
                Session["svConfirmationNum"] = checkOutReservation.confirmationNum;
                int rt = checkOutReservation.confirmationNum;
                if (rt == 0)
                {
                    Session["svRoomType"] = "King";
                }
                else if (rt == 1)
                {
                    Session["svRoomType"] = "Queen";
                }
                else if (rt == 2)
                {
                    Session["svRoomType"] = "Double";
                }
                else
                {
                    Session["svRoomType"] = "Suite";
                }
                Session["svCheckInDate"] = checkOutReservation.checkInDate;
                Session["svCheckOutDate"] = checkOutReservation.checkOutDate;
                Session["svBillingFirstName"] = checkOutReservation.billingFirstName;
                Session["svBillingLastName"] = checkOutReservation.billingLastName;
                Session["svPhone"] = checkOutReservation.phone;
                Session["svEmail"] = checkOutReservation.email;

                double duration = (DateTime.Now - checkOutReservation.checkInDate).TotalDays - 1;
                if (duration <= 0)
                {
                    duration = 1;
                }
                double price = 0;
                if (checkOutReservation.Room_type.type == 0)
                {
                    price = 100;
                }
                else if (checkOutReservation.Room_type.type == 1)
                {
                    price = 150;
                }
                else if (checkOutReservation.Room_type.type == 2)
                {
                    price = 200;
                }
                else
                {
                    price = 300;
                }
                Session["payment"] = duration * price;
                return RedirectToAction("GenerateBill");
            }
        }

        public ActionResult GenerateBill()
        {
            return View();
        }

        public ActionResult Occupancy()
        {
            return View();
        }

        public ActionResult CurrentOccupancy()
        {
            double kcount = 0;
            double kinv = 0;
            double qcount = 0;
            double qinv = 0;
            double dcount = 0;
            double dinv = 0;
            double scount = 0;
            double sinv = 0;

            // get the values of inventory
            using (var context = new HotelDataModel())
            {
                var room_query0 = from r in context.RoomSet
                                  where r.type == 0
                                  select r;
                var room0 = room_query0.FirstOrDefault();
                if (room0 == null)
                { kcount = 0; kinv = 0; }
                else { kinv = room0.inventory; }

                var room_query1 = from r in context.RoomSet
                                  where r.type == 1
                                  select r;
                var room1 = room_query1.FirstOrDefault();
                if (room1 == null)
                { qcount = 0; qinv = 0; }
                else { qinv = room1.inventory; }

                var room_query2 = from r in context.RoomSet
                                  where r.type == 2
                                  select r;
                var room2 = room_query2.FirstOrDefault();
                if (room2 == null)
                { dcount = 0; dinv = 0; }
                else { dinv = room2.inventory; }

                var room_query3 = from r in context.RoomSet
                                  where r.type == 3
                                  select r;
                var room3 = room_query3.FirstOrDefault();
                if (room3 == null)
                { scount = 0; sinv = 0; }
                else { sinv = room3.inventory; }

                // get the value of counts
                foreach (Room r in context.RoomSet)
                {
                    if (r.date.Year == DateTime.Now.Year &&
                        r.date.Month == DateTime.Now.Month &&
                        r.date.Day == DateTime.Now.Day)
                    {
                        if (r.type == 0) { kcount = r.occupancy; }
                        else if (r.type == 1) { qcount = r.occupancy; }
                        else if (r.type == 2) { dcount = r.occupancy; }
                        else { scount = r.occupancy; }
                    }
                }

                double kpercent = 0;
                if (kinv == 0) { kpercent = 0; }
                else { kpercent = kcount / kinv; }

                double qpercent = 0;
                if (qinv == 0) { qpercent = 0; }
                else { qpercent = qcount / qinv; }

                double dpercent = 0;
                if (dinv == 0) { dpercent = 0; }
                else { dpercent = dcount / dinv; }

                double spercent = 0;
                if (sinv == 0) { spercent = 0; }
                else { spercent = scount / sinv; }

                ViewBag.kcount = kcount;
                ViewBag.qcount = qcount;
                ViewBag.dcount = dcount;
                ViewBag.scount = scount;

                ViewBag.kpercent = kpercent;
                ViewBag.qpercent = qpercent;
                ViewBag.dpercent = dpercent;
                ViewBag.spercent = spercent;
            }
            return View();
        }

        public ActionResult FutureOccupancy()
        {
            return View();
        }

        public ActionResult FOQuery(FOccupancy model)
        {
            double kcount = 0;
            double kinv = 0;
            double qcount = 0;
            double qinv = 0;
            double dcount = 0;
            double dinv = 0;
            double scount = 0;
            double sinv = 0;

            // get the values of inventory
            using (var context = new HotelDataModel())
            {
                var room_query0 = from r in context.RoomSet
                                  where r.type == 0
                                  select r;
                var room0 = room_query0.FirstOrDefault();
                if (room0 == null)
                { kcount = 0; kinv = 0; }
                else { kinv = room0.inventory; }

                var room_query1 = from r in context.RoomSet
                                  where r.type == 1
                                  select r;
                var room1 = room_query1.FirstOrDefault();
                if (room1 == null)
                { qcount = 0; qinv = 0; }
                else { qinv = room1.inventory; }

                var room_query2 = from r in context.RoomSet
                                  where r.type == 2
                                  select r;
                var room2 = room_query2.FirstOrDefault();
                if (room2 == null)
                { dcount = 0; dinv = 0; }
                else { dinv = room2.inventory; }

                var room_query3 = from r in context.RoomSet
                                  where r.type == 3
                                  select r;
                var room3 = room_query3.FirstOrDefault();
                if (room3 == null)
                { scount = 0; sinv = 0; }
                else { sinv = room3.inventory; }

                // get the value of counts
                foreach (Room r in context.RoomSet)
                {
                    if (r.date.Year == model.Year &&
                        r.date.Month == model.Month &&
                        r.date.Day == model.Day)
                    {
                        if (r.type == 0) { kcount = r.occupancy; }
                        else if (r.type == 1) { qcount = r.occupancy; }
                        else if (r.type == 2) { dcount = r.occupancy; }
                        else { scount = r.occupancy; }
                    }
                }

                double kpercent = 0;
                if (kinv == 0) { kpercent = 0; }
                else { kpercent = kcount / kinv; }

                double qpercent = 0;
                if (qinv == 0) { qpercent = 0; }
                else { qpercent = qcount / qinv; }

                double dpercent = 0;
                if (dinv == 0) { dpercent = 0; }
                else { dpercent = dcount / dinv; }

                double spercent = 0;
                if (sinv == 0) { spercent = 0; }
                else { spercent = scount / sinv; }

                ViewBag.kcount = kcount;
                ViewBag.qcount = qcount;
                ViewBag.dcount = dcount;
                ViewBag.scount = scount;

                ViewBag.kpercent = kpercent;
                ViewBag.qpercent = qpercent;
                ViewBag.dpercent = dpercent;
                ViewBag.spercent = spercent;
            }
            return View();
        }

        public ActionResult TodayCheckIn()
        {
            using (var context = new HotelDataModel())
            {
                var checkInReservation_query = from r in context.ReservationSet
                                               where r.checkInDate.Year == DateTime.Now.Year &&
                                               r.checkInDate.Month == DateTime.Now.Month
                                               && r.checkInDate.Day == DateTime.Now.Day
                                               && r.checkedIn == false
                                               select r;
                // var checkInReservation = checkInReservation_query.FirstOrDefault();
                //   HashSet<Reservation> rs = new HashSet<Reservation>();
                //   rs.Add(checkInReservation);
                //   return View(rs);
                HashSet<Reservation> hs = new HashSet<Reservation>();
                foreach (Reservation rr in checkInReservation_query)
                {
                    hs.Add(rr);
                }
                return View(hs);
            }
        }

        public ActionResult TodayCheckOut()
        {
            using (var context = new HotelDataModel())
            {
                /* var checkInReservation_query = from r in context.ReservationSet
                                                where r.checkInDate.Year == DateTime.Now.Year &&
                                                r.checkInDate.Month == DateTime.Now.Month
                                                && r.checkInDate.Day == DateTime.Now.Day
                                                && r.checkedIn == false
                                                select r;
                 // var checkInReservation = checkInReservation_query.FirstOrDefault();
                 //   HashSet<Reservation> rs = new HashSet<Reservation>();
                 //   rs.Add(checkInReservation);
                 //   return View(rs);
                 */

                HashSet<Reservation> hs = new HashSet<Reservation>();
                foreach (Reservation r in context.ReservationSet)
                {
                    if (r.checkOutDate.Year == DateTime.Now.Year &&
                                                r.checkOutDate.Month == DateTime.Now.Month
                                                && r.checkOutDate.Day == DateTime.Now.Day
                                                && r.checkedIn == true
                                                && r.checkedOut == false)
                    {
                       // if (DateTime.Now.Hour >= 14)
                       // {
                       //     r.checkedOut = true;
                        //    r.checkOutDate = DateTime.Now;
                      //  }
                      //  else
                      //  {
                            hs.Add(r);
                      //  }
                    }
                }
                return View(hs);
            }
        }

        public ActionResult AutoCheckOut()
        {
            using (var context = new HotelDataModel())
            {
                foreach (Reservation r in context.ReservationSet)
                {
                    if (r.checkOutDate.Year == DateTime.Now.Year &&
                                                r.checkOutDate.Month == DateTime.Now.Month
                                                && r.checkOutDate.Day == DateTime.Now.Day
                                                && r.checkedOut == false)
                    {
                        Console.WriteLine(DateTime.Now.Hour);
                        if (DateTime.Now.Hour >= 14)
                        {
                            
                            r.checkedOut = true;
                            r.checkOutDate = DateTime.Now;
                        }
                    }
                    else if (r.checkedOut == false && r.checkOutDate.CompareTo(DateTime.Now) <= 0)
                    {
                        r.checkedOut = true;
                        r.checkOutDate = DateTime.Now;
                    }
                }
                context.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}