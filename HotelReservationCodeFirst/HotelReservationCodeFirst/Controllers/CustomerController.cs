﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using HotelReservationCodeFirst.Models;
using DataModel;
using System.Collections.Generic;
using BizLogic;


namespace HotelReservationCodeFirst.Controllers
{
    public class CustomerController : AccountController
    {
        HotelReservationExtensions ext = new HotelReservationExtensions();
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ManageReservations()
        {
            string userId = User.Identity.GetUserId();
            var user = UserManager.FindById(userId); 

            //if (user.reservations == null)
            //{
            //    user.reservations = new List<Reservation>();
            //}

            List<Reservation> res = ext.getAllReservationsForUser(userId);
            user.reservations = res;
            return View(user);
        }

        public ActionResult ViewReservation(Reservation r)
        {
            return View(r);
        }

        public ActionResult CancelReservation(Reservation model)
        {
            var confirmationNum = model.confirmationNum;
            ext.cancelReservation(confirmationNum);
            return RedirectToAction("ManageReservations");
        }

        public void addReservation(string userId, Reservation r)
        {
            var user = UserManager.FindById(userId);
            user.reservations.Add(r);
        }
    }
}