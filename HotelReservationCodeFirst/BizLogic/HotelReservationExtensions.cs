﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Security.Principal;
//using System.Data.Entity;
//using System.Data.Entity.Infrastructure;

using DataModel; // remember to add referrence to DataModel
//using System.Data.Entity.Validation;
using System.Diagnostics;

namespace BizLogic
{
    public class HotelReservationExtensions
    {

        public void addRoom()
        {
            using(var context = new HotelDataModel())
            {
                if (context.RoomSet.Count() == 0)
                {
                    Room king = new Room();     //0
                    king.basicPrice = 200;
                    king.occupancy = 0;
                    king.maxGuest = 4;
                    king.description = "description";
                    king.amanities = "amanities";
                    king.inventory = 100;
                    king.date = DateTime.Now.Date;
                    king.type = 0;

                    Room queen = new Room();    //1
                    queen.basicPrice = 180;
                    queen.occupancy = 0;
                    queen.maxGuest = 4;
                    queen.description = "description";
                    queen.amanities = "amanities";
                    queen.inventory = 100;
                    queen.date = DateTime.Now.Date;
                    queen.type = 1;

                    Room dbl = new Room();      //2
                    dbl.basicPrice = 150;
                    dbl.occupancy = 0;
                    dbl.maxGuest = 2;
                    dbl.description = "description";
                    dbl.amanities = "amanities";
                    dbl.inventory = 100;
                    dbl.date = DateTime.Now.Date;
                    dbl.type = 2;

                    Room suite = new Room();    //3
                    suite.basicPrice = 100;
                    suite.occupancy = 0;
                    suite.maxGuest = 2;
                    suite.description = "description";
                    suite.amanities = "amanities";
                    suite.inventory = 100;
                    suite.date = DateTime.Now.Date;
                    suite.type = 3;

                    context.RoomSet.Add(king);
                    context.RoomSet.Add(queen);
                    context.RoomSet.Add(dbl);
                    context.RoomSet.Add(suite);


                }
                context.SaveChanges();
            }
        }

       

        public void customerCancelReservation(int confirmationNum)
        {
            using (var context = new HotelDataModel())
            {
                try
                {
                    var reserve = (Reservation)from r in context.ReservationSet
                                               where r.confirmationNum == confirmationNum
                                               select r;

                    if (!reserve.checkedIn)
                        context.ReservationSet.Remove(reserve);
                    else
                    {
                        Console.WriteLine("Customer already checked in. Cancel reservation failed!");
                    }

                }
                catch (Exception e)
                {

                }
            }
        }


        public void customerMaintainProfile()
        {

        }

        //create a new room according to the room type
        public Room createRoom(int type, DateTime i)
        {
            Room king = new Room();     //0
            king.basicPrice = 200;
            king.occupancy = 0;
            king.maxGuest = 4;
            king.description = "description";
            king.amanities = "amanities";
            king.inventory = 100;
            king.date = i;
            king.type = 0;

            Room queen = new Room();    //1
            queen.basicPrice = 180;
            queen.occupancy = 0;
            queen.maxGuest = 4;
            queen.description = "description";
            queen.amanities = "amanities";
            queen.inventory = 100;
            queen.date = i;
            queen.type = 1;

            Room dbl = new Room();      //2
            dbl.basicPrice = 150;
            dbl.occupancy = 0;
            dbl.maxGuest = 2;
            dbl.description = "description";
            dbl.amanities = "amanities";
            dbl.inventory = 100;
            dbl.date = i;
            dbl.type = 2;

            Room suite = new Room();    //3
            suite.basicPrice = 100;
            suite.occupancy = 0;
            suite.maxGuest = 2;
            suite.description = "description";
            suite.amanities = "amanities";
            suite.inventory = 100;
            suite.date = i;
            suite.type = 3;

            if (type == 0)
                return king;
            else if (type == 1)
                return queen;
            else if (type == 2)
                return dbl;
            else
                return suite;
        }

        public Reservation customerReserve(int confirmationNum, DateTime checkInDate,
            DateTime checkOutDate, bool checkedIn, bool checkedOut, string address,
            string city, string state, string zip, string email, string phone,
            string billingFirstName, string billingLastName, string firstName2,
            string lastName2, string firstName3,
            string lastName3, string firstName4, string lastName4, int Room_type, string userId)
        {
            using (var context = new HotelDataModel())
            {
                var reservation = new Reservation();
                reservation.confirmationNum = confirmationNum;
                reservation.checkInDate = checkInDate;
                reservation.checkOutDate = checkOutDate;
                reservation.checkedIn = checkedIn;
                reservation.checkedOut = checkedOut;
                reservation.address = address;
                reservation.city = city;
                reservation.state = state;
                reservation.zip = zip;
                reservation.email = email;
                reservation.phone = phone;
                reservation.billingFirstName = billingFirstName;
                reservation.billingLastName = billingLastName;
                reservation.firstName2 = firstName2;
                reservation.lastName2 = lastName2;
                reservation.firstName3 = firstName3;
                reservation.firstName4 = firstName4;
                reservation.lastName2 = lastName2;
                reservation.lastName4 = lastName4;

                //reservation.Room_type cannot be null!
                if (reservation.Room_type != null)
                {
                    reservation.Room_type.type = Room_type;
                }
                else
                {
                    Room r = createRoom(Room_type, checkInDate);
                    reservation.Room_type = r;
                }

                reservation.ApplicationUser_Id = userId;
                context.ReservationSet.Add(reservation);
                context.SaveChanges();

                return reservation;
            }
        }

        public void staffViewInventory()
        {
            //try
            //{
            using (var context = new HotelDataModel())
            {
                var rooms = context.RoomSet;
                Console.WriteLine("Printing hotel inventories:");
                foreach (var r in rooms)
                {
                    switch (r.type)
                    {
                        case 0: Console.WriteLine("King: "); break;
                        case 1: Console.WriteLine("Quees: "); break;
                        case 2: Console.WriteLine("Double: "); break;
                        case 3: Console.WriteLine("Suit: "); break;
                    }
                    Console.WriteLine(r.inventory);
                }
            }
            //}
            //catch(Exception e)
            //{

            //}

        }

        public void staffUpdateInventory(int roomType, int newInv)
        {
            using (var context = new HotelDataModel())
            {
                try
                {
                    var room = context.RoomSet.Find(roomType);
                    room.inventory = newInv;
                    context.SaveChanges();
                    room = context.RoomSet.Find(roomType);
                    Console.WriteLine("updated room inventory: type: {0}, inventory: {1}", roomType, room.inventory);
                }
                catch (Exception e) { }
            }
        }

        public void staffViewOccupancyPercentage(DateTime date)
        {
            using (var context = new HotelDataModel())
            {
                var reservations = context.ReservationSet.Where(r => r.checkInDate.CompareTo(date) <= 0 && r.checkOutDate.CompareTo(date) >= 0);

                var rooms = context.RoomSet;
                foreach (var r in rooms)
                {
                    double occupancy = 0.0;
                    switch (r.type)
                    {
                        case 0:
                            occupancy = (double)reservations.Count(rm => rm.Room_type.type == 0);
                            Console.WriteLine("King: ");
                            break;
                        case 1:
                            occupancy = (double)reservations.Count(rm => rm.Room_type.type == 1);
                            Console.WriteLine("Quees: ");
                            break;
                        case 2:
                            occupancy = (double)reservations.Count(rm => rm.Room_type.type == 2);
                            Console.WriteLine("Double: ");
                            break;
                        case 3:
                            occupancy = (double)reservations.Count(rm => rm.Room_type.type == 3);
                            Console.WriteLine("Suit: ");
                            break;
                    }
                    Console.WriteLine(occupancy / (double)r.inventory);
                }
            }
        }

        public void staffViewUpcomingCheckIn()
        {
            using (var context = new HotelDataModel())
            {
                var upcomings = context.ReservationSet.Where(r => r.checkInDate.Equals(DateTime.Today));
                Console.WriteLine("Customers expected to check in:");
                foreach (var r in upcomings)
                {
                    Console.WriteLine("{0}, {1}", r.billingFirstName, r.billingLastName);
                }
            }
        }

        public void staffViewUpcomingCheckOut()
        {
            using (var context = new HotelDataModel())
            {
                try
                {
                    var upcomings = context.ReservationSet.Where(r => r.checkOutDate.Equals(DateTime.Today));
                    Console.WriteLine("Customers expected to check out:");
                    foreach (var r in upcomings)
                    {
                        Console.WriteLine("{0}, {1}", r.billingFirstName, r.billingLastName);
                        TimeSpan after = new TimeSpan(14, 0, 0);
                        TimeSpan now = DateTime.Now.TimeOfDay;
                        if (now >= after)
                        {
                            staffCheckInCustomer(r.confirmationNum);
                        }
                    }
                }
                catch (Exception e) { }
            }
        }

        public void staffCheckInCustomer(int confNum)
        {
            using (var context = new HotelDataModel())
            {
                try
                {
                    var reserv = context.ReservationSet.Find(confNum);
                    if (reserv == null)
                    {
                        Console.WriteLine("cannot find reservation with provided confirmation number");
                    }
                    var room = context.RoomSet.Find(reserv.Room_type);
                    if (DateTime.Today.CompareTo(reserv.checkInDate) < 0)
                    {
                        // cannot check in before check-in date
                        Console.WriteLine("cannot check in before the check-in date");
                        return;
                    }
                    if (room.occupancy < room.inventory)
                    {
                        reserv.checkedIn = true;
                        room.occupancy += 1;
                    }
                    else
                    {
                        Console.WriteLine("all rooms are occupied");
                    }
                    context.SaveChanges();
                }
                catch (Exception e) { }
            }
        }

        public void staffCheckOutCustomer(int confNum)
        {
            using (var context = new HotelDataModel())
            {
                try
                {
                    var reserv = context.ReservationSet.Find(confNum);
                    if (reserv == null)
                    {
                        Console.WriteLine("cannot find reservation with provided confirmation number");
                    }
                    reserv.checkedOut = true;
                    var room = context.RoomSet.Find(reserv.Room_type);
                    room.occupancy -= 1;
                    context.SaveChanges();
                }
                catch (Exception e) { }

            }

        }

        public List<Reservation> getAllReservationsForUser(string userId)
        {
            using(var ctx = new HotelDataModel())
            {
                List<Reservation> res =  ctx.ReservationSet.Where(r => r.ApplicationUser_Id == userId ).ToList();
                return res;
            }
        }

        public void cancelReservation(int confirmationNum)
        {
            using(var ctx = new HotelDataModel())
            {
                Reservation r = ctx.ReservationSet.Find(confirmationNum);
                if(r != null)
                {
                    ctx.ReservationSet.Remove(r);
                    ctx.SaveChanges();
                }
            }
        }

        //get the available number of rooms of room type specified
        public int roomAvailable(int type, DateTime checkIn, DateTime checkOut)
        {
            int min = Int32.MaxValue;
            using (var context = new HotelDataModel())
            {
                var roomQuery = from r in context.RoomSet
                                where r.date >= checkIn && r.date <= checkOut && r.type == type
                                select r;

                foreach(Room r in roomQuery)
                {
                    int diff = r.inventory - r.occupancy;
                    if(diff < min)
                    {
                        min = diff;
                    }
                }
            }

            return min;
        }


        public double getRateForDay(int roomType, DateTime date)
        {
            int occupancy = roomAvailable(roomType, date, date);
            using (var ctx = new HotelDataModel())
            {
                Room r = ctx.RoomSet.Where(m => m.type == roomType).First();
                return r.basicPrice * 2 * r.inventory / occupancy;
            }
        }


        public double getTotalRateForDayRange(int roomType, DateTime start, DateTime end)
        {
            double total = 0.0;
            for (DateTime i = start; i.CompareTo(end) < 0; i = i.AddDays(1.0))
            {
                total += getRateForDay(roomType, i);
            }
            return total;
        }

        //add room series from check in date to check out date
        public void addRoomSeries(int type, DateTime checkIn, DateTime checkOut)
        {
            using (var context = new HotelDataModel())
            {
                for(DateTime i = checkIn; i.CompareTo(checkOut) < 0;i = i.AddDays(1.0))
                {
                    int year = i.Year;
                    int month = i.Month;
                    int day = i.Day;

                    int count = 0;

                    //program stuck here
                    count = (from r in context.RoomSet
                             where r.type == type && r.date.Year == year && r.date.Month == month && r.date.Day == day
                             select r).Count();


                    //count = context.RoomSet.Count(r => r.type == type && r.date.Year == year && r.date.Month == month && r.date.Day == day);

                    //foreach(Room r in context.RoomSet)
                    //{
                    //    if (r.type == type && r.date.Year == year && r.date.Month == month && r.date.Day == day)
                    //        count++;
                    //}

                    if (count == 0)
                    {
                        //add the new room to the database
                        Room king = new Room();     //0
                        king.basicPrice = 200;
                        king.occupancy = 0;
                        king.maxGuest = 4;
                        king.description = "description";
                        king.amanities = "amanities";
                        king.inventory = 100;
                        king.date = i;
                        king.type = 0;

                        Room queen = new Room();    //1
                        queen.basicPrice = 180;
                        queen.occupancy = 0;
                        queen.maxGuest = 4;
                        queen.description = "description";
                        queen.amanities = "amanities";
                        queen.inventory = 100;
                        queen.date = i;
                        queen.type = 1;

                        Room dbl = new Room();      //2
                        dbl.basicPrice = 150;
                        dbl.occupancy = 0;
                        dbl.maxGuest = 2;
                        dbl.description = "description";
                        dbl.amanities = "amanities";
                        dbl.inventory = 100;
                        dbl.date = i;
                        dbl.type = 2;

                        Room suite = new Room();    //3
                        suite.basicPrice = 100;
                        suite.occupancy = 0;
                        suite.maxGuest = 2;
                        suite.description = "description";
                        suite.amanities = "amanities";
                        suite.inventory = 100;
                        suite.date = i;
                        suite.type = 3;

                        switch(type)
                        {
                            case 0: context.RoomSet.Add(king) ;break;
                            case 1: context.RoomSet.Add(queen);break;
                            case 2: context.RoomSet.Add(dbl);break;
                            case 3: context.RoomSet.Add(suite); break;
                            default:;break;
                        }

                        context.SaveChanges();
                    }
                    else
                    {
                        continue;
                    }
                }

            }
        }

        //change the invertory of room after reservation
        public void changeOccupancy(int type, DateTime checkIn, DateTime checkOut)
        {
            using (var context = new HotelDataModel())
            {
                foreach(Room r in context.RoomSet)
                {
                    if(r.date >= checkIn && r.date <= checkOut)
                    {
                        if(r.occupancy < r.inventory)
                        {
                            r.occupancy = r.occupancy + 1;
                        }
                    }
                }

                context.SaveChanges();
            }
        }

    }
}
